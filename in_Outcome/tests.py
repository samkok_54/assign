from django.core.urlresolvers import resolve
from django.http import HttpRequest
from django.template.loader import render_to_string
from django.test import TestCase
import datetime
from in_Outcome.models import Item
from in_Outcome.views import home_page
import pep8
import unittest


class TestCodeFormat(unittest.TestCase):
    def test_pep8_unittest(self):
        """Test that we conform to PEP8."""
        fchecker = pep8.Checker('in_Outcome/tests.py', show_source=True)
        file_errors = fchecker.check_all()
        print("\nunittest found %s errors (and warnings)" % file_errors)

    def test_pep8_view(self):
        """Test that we conform to PEP8."""
        fchecker = pep8.Checker('in_Outcome/views.py', show_source=True)
        file_errors = fchecker.check_all()
        print("\nview found %s errors (and warnings)" % file_errors)

    def test_pep8_model(self):
        """Test that we conform to PEP8."""
        fchecker = pep8.Checker('in_Outcome/models.py', show_source=True)
        file_errors = fchecker.check_all()
        print("\nmodel found %s errors (and warnings)" % file_errors)

    def test_pep8_functionaltest(self):
        """Test that we conform to PEP8."""
        fchecker = pep8.Checker('functional_tests/tests.py', show_source=True)
        file_errors = fchecker.check_all()
        print("\nfunctionaltest found %s errors (and warnings)" % file_errors)


class HomePageTest(TestCase):
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_show_list_items(self):
        Item.objects.create(date='2012-12-03', text='Income', money='50000')
        Item.objects.create(date='2014-02-03', text='buy jar', money='-50')
        Item.objects.create()
        request = HttpRequest()
        response = home_page(request)
        self.assertIn('2012-12-03', response.content.decode())
        self.assertIn('Income', response.content.decode())
        self.assertIn('50000.0', response.content.decode())
        self.assertIn('2014-02-03', response.content.decode())
        self.assertIn('buy jar', response.content.decode())
        self.assertIn('-50.0', response.content.decode())
        self.assertIn('49950.0', response.content.decode())  # check total
        Day = datetime.date.today()
        self.assertIn(Day.strftime('%Y-%m-%d'), response.content.decode())
        self.assertIn('-----------------', response.content.decode())
        self.assertIn('0.0', response.content.decode())

    def test_home_page_can_save_a_POST_request(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['date_text'] = '2012-12-03'
        request.POST['item_text'] = 'Buy Bed'
        request.POST['money_text'] = '-12300'
        response = home_page(request)
        # #check data have 1 item after save
        self.assertEqual(Item.objects.count(), 1)
        new_item = Item.objects.first()
        self.assertEqual(new_item.date, '2012-12-03')
        self.assertEqual(new_item.text, 'Buy Bed')
        self.assertEqual(new_item.money, -12300.0)

    def test_home_page_only_saves_items_when_necessary(self):
        request = HttpRequest()
        home_page(request)
        self.assertEqual(Item.objects.count(), 0)


class ItemModelTest(TestCase):
    def test_saving_and_retrieving_items(self):
        first_item = Item()
        first_item.date = '2012-12-22'
        first_item.text = 'income'
        first_item.money = '50000'
        first_item.save()
        second_item = Item()
        second_item.date = '2012-02-23'
        second_item.text = 'buy pork'
        second_item.money = '-250'
        second_item.save()
        saved_items = Item.objects.all()
        self.assertEqual(saved_items.count(), 2)
        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]
        self.assertEqual(first_saved_item.date, '2012-12-22')
        self.assertEqual(first_saved_item.text, 'income')
        self.assertEqual(first_saved_item.money, 50000.0)
        self.assertEqual(second_saved_item.date, '2012-02-23')
        self.assertEqual(second_saved_item.text, 'buy pork')
        self.assertEqual(second_saved_item.money, -250.0)
