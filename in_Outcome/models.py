from django.db import models
import datetime


class Item(models.Model):
    Day = datetime.date.today()
    date = models.TextField(default=Day.strftime('%Y-%m-%d'))
    text = models.TextField(default='-----------------')
    money = models.FloatField(default=0.0)
