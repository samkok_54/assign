from django.shortcuts import redirect, render
from in_Outcome.models import Item
import datetime


def home_page(request):
    if request.method == 'POST':
        if request.POST.get('delete', '') == 'delete':
            deletetext = int(request.POST['item_delete'])
            Item.objects.get(id=deletetext).delete()
        else:
            if request.POST.get('date_text', '') == '':
                Day = datetime.date.today()
            else:
                Day = request.POST.get('date_text')
            if request.POST.get('item_text', '') == '':
                Event = '-----------------'
            else:
                Event = request.POST.get('item_text', '')
            if request.POST.get('money_text', '') == '':
                moneyy = 0.0
            else:
                moneyy = float(request.POST.get('money_text', ''))
            Item.objects.create(date=Day, text=Event, money=moneyy)
    items = Item.objects.all()
    total = 0.0
    if Item.objects.count() != 0:
        for item in items:
            total = total + float(item.money)
    current = request.POST.get('current', 'home')
    return render(request, 'home.html', {'items': items,
                                         'totals': total,
                                         'current': current})
