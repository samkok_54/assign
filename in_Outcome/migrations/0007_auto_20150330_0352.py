# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('in_Outcome', '0006_auto_20150329_1530'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='item',
            name='list',
        ),
        migrations.DeleteModel(
            name='List',
        ),
        migrations.AlterField(
            model_name='item',
            name='date',
            field=models.TextField(default='2015-03-30'),
            preserve_default=True,
        ),
    ]
