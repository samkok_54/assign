# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('in_Outcome', '0008_auto_20150401_1321'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='date',
            field=models.TextField(default='2015-04-08'),
            preserve_default=True,
        ),
    ]
