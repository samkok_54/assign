# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('in_Outcome', '0002_remove_totals_text'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='date',
            field=models.TextField(default='2015-03-28'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='item',
            name='money',
            field=models.TextField(default='0.0'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='item',
            name='text',
            field=models.TextField(default='-----------------'),
            preserve_default=True,
        ),
    ]
