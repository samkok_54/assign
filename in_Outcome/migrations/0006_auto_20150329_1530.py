# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('in_Outcome', '0005_remove_item_count'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Totals',
            new_name='List',
        ),
        migrations.AddField(
            model_name='item',
            name='list',
            field=models.ForeignKey(to='in_Outcome.List', default=None),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='item',
            name='date',
            field=models.TextField(default='2015-03-29'),
            preserve_default=True,
        ),
    ]
