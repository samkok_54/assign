# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('in_Outcome', '0009_auto_20150408_0234'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='money',
            field=models.FloatField(default=0.0),
            preserve_default=True,
        ),
    ]
