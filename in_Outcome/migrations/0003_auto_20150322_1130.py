# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('in_Outcome', '0002_auto_20150322_0928'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='date',
            field=models.TextField(default='12/2/56'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='item',
            name='money',
            field=models.TextField(default='00.00'),
            preserve_default=True,
        ),
    ]
