# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('in_Outcome', '0004_item_count'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='item',
            name='count',
        ),
    ]
