from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import time
import datetime


class NewVisitorTest(StaticLiveServerTestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        # self.browser.quit()
        pass

    def check_for_row(self, row_text, idT):
        table = self.browser.find_element_by_id(idT)
        rows = table.find_elements_by_tag_name('td')
        self.assertIn(row_text, [row.text for row in rows])

    def NOT_intable(self, row_date, row_text, row_money):
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('td')
        c1 = any((l == row_text for l in [row.text for row in rows]))
        c2 = any((l == row_date for l in [row.text for row in rows]))
        c3 = any((l == row_money for l in [row.text for row in rows]))
        return c1 and c2 and c3

    def test_can_start_a_list_and_retrieve_it_later(self):
        # open browser
        self.browser.get(self.live_server_url)
        self.assertIn('assignment', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Income-Outcome', header_text)
        # #first times
        self.browser.find_element_by_name('edit').click()
        inputdate = self.browser.find_element_by_id('date_new_item')
        inputitem = self.browser.find_element_by_id('id_new_item')
        inputmoney = self.browser.find_element_by_id('money_new_item')
        self.assertEqual(inputdate.get_attribute('placeholder'),
                         'date of event YYYY-MM-DD')
        self.assertEqual(inputitem.get_attribute('placeholder'),
                         'Enter a item')
        self.assertEqual(inputmoney.get_attribute('placeholder'),
                         '00.00')
        # #input first values
        inputdate.send_keys('2015-02-03')
        inputitem.send_keys('incomes')
        inputmoney.send_keys('90000')
        # #check table of first value
        self.browser.find_element_by_id('submit_data').click()
        self.browser.find_element_by_name('show').click()
        self.check_for_row('2015-02-03', 'id_list_table')
        self.check_for_row('incomes', 'id_list_table')
        self.check_for_row('90000.0', 'id_list_table')
        # #check total 1
        self.check_for_row('90000.0', 'total_list')
        # ####second times for default
        time.sleep(1)
        self.browser.find_element_by_name('edit').click()
        inputdate = self.browser.find_element_by_id('date_new_item')
        inputitem = self.browser.find_element_by_id('id_new_item')
        inputmoney = self.browser.find_element_by_id('money_new_item')
        Day = datetime.date.today()
        # #check table of second value
        self.browser.find_element_by_id('submit_data').click()
        self.browser.find_element_by_name('show').click()
        self.check_for_row(Day.strftime('%Y-%m-%d'), 'id_list_table')
        self.check_for_row('-----------------', 'id_list_table')
        self.check_for_row('0.0', 'id_list_table')
        # #check total 2
        self.check_for_row('90000.0', 'total_list')
        # #click delete
        self.browser.find_element_by_name('show').click()
        self.browser.find_element_by_id('button_d_1').click()
        # #check for delete data
        self.browser.find_element_by_name('show').click()
        day = '2015-02-03'
        text = 'incomes'
        mon = '90000.0'
        self.assertEqual(self.NOT_intable(day, text, mon), False)
        # #check total after delete
        self.check_for_row('0.0', 'total_list')
        # close browser
        self.browser.quit()
        # open browser
        self.browser = webdriver.Firefox()
        self.browser.get(self.live_server_url)
        # ########################################
        # #########CHECK SECOND BROWSER###########
        self.assertIn('assignment', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Income-Outcome', header_text)
        # #first times
        self.browser.find_element_by_name('edit').click()
        inputdate = self.browser.find_element_by_id('date_new_item')
        inputitem = self.browser.find_element_by_id('id_new_item')
        inputmoney = self.browser.find_element_by_id('money_new_item')
        self.assertEqual(inputdate.get_attribute('placeholder'),
                         'date of event YYYY-MM-DD')
        self.assertEqual(inputitem.get_attribute('placeholder'),
                         'Enter a item')
        self.assertEqual(inputmoney.get_attribute('placeholder'),
                         '00.00')
        # input first values
        inputdate.send_keys('2009-12-24')
        inputitem.send_keys('incomes')
        inputmoney.send_keys('90000')
        # check table of first value
        self.browser.find_element_by_id('submit_data').click()
        self.browser.find_element_by_name('show').click()
        self.check_for_row('2009-12-24', 'id_list_table')
        self.check_for_row('incomes', 'id_list_table')
        self.check_for_row('90000.0', 'id_list_table')
        # # check total 1
        self.check_for_row('90000.0', 'total_list')
        # ####second times
        time.sleep(1)
        self.browser.find_element_by_name('edit').click()
        inputdate = self.browser.find_element_by_id('date_new_item')
        inputitem = self.browser.find_element_by_id('id_new_item')
        inputmoney = self.browser.find_element_by_id('money_new_item')
        # #input second values
        inputitem.send_keys('Buy Pen')
        inputmoney.send_keys('-10')
        # #check table of second value
        Day = datetime.date.today()
        self.browser.find_element_by_id('submit_data').click()
        self.browser.find_element_by_name('show').click()
        self.check_for_row(Day.strftime('%Y-%m-%d'),
                           'id_list_table')  # default date
        self.check_for_row('Buy Pen', 'id_list_table')
        self.check_for_row('-10.0', 'id_list_table')
        # #check total 2
        self.check_for_row('89990.0', 'total_list')
        # #click delete
        self.browser.find_element_by_name('show').click()
        self.browser.find_element_by_id('button_d_2').click()
        # #check for delete data
        self.browser.find_element_by_name('show').click()
        # self.check_for_row_in_list_table('-----------------')
        data = Day.strftime('%Y-%m-%d')
        checkdate = self.NOT_intable(data, '-----------------', '0.0')
        self.assertEqual(checkdate, False)
        # #check total after delete
        self.check_for_row('89990.0', 'total_list')
        self.browser.find_element_by_name('home').click()
        self.fail('Finish the test!')

if __name__ == '__main__':
    unittest.main(warnings='ignore')
